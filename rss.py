import fetcher, rss_parser

if __name__ == '__main__':
	import sys
	if len(sys.argv) != 2:
		print('Usage:\n\t%s <rss-feed-url>\n\nSample RSS feed:\n'
				'\thttp://www.rssboard.org/files/sample-rss-2.xml\n' % (sys.argv[0]))
	else:
		xml = fetcher.fetch_url(sys.argv[1])
		if xml:
			rss = rss_parser.parse_string(xml)
			print(rss)
			print(rss.channel)
			for item in rss.channel.items:
				print(item)