Introduction
------------

This project was started for educational purposes and doesn't intend to be usable by end-user.

`rss.py` contains the main entry point and its usage is as follows:

        Usage:
        	rss.py <rss-feed-url>
        
        Sample RSS feed:
        	http://www.rssboard.org/files/sample-rss-2.xml

Technical notes
---------------

The Python version used among the project is 2.7.
No third party libraries are necessary.

Architectural notes
-------------------

`fetcher.py` is a simple wrapper over `urllib`. It provides one function `fetch_url(url)` returning a string with the fetched data.

`rss_parser.py` is the "brains" that understand the RSS XML structure. It provides `parse_string(xml)` function taking the RSS XML string and returning a specific representation of the feed.

And finally, `rss.py` is the glue for all that with some user-friendly facilities.