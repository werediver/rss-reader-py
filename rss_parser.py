# RSS 2.0 parser
# Specification: http://www.rssboard.org/rss-specification

# See http://docs.python.org/library/xml.etree.elementtree.html
import xml.etree.ElementTree as etree


class RssBase(object):
	# XML element associated with the instance
	element = None

	def update_field(self, field_name):
		field_val_src = self.element.find(field_name)
		field_val = field_val_src.text if field_val_src != None else ''
		setattr(self, field_name, field_val)

	def __unicode__(self):
		s = ''
		if type(self.element) == etree.Element:
			s += '%s: %s\n' % (type(self).__name__, self.__dict__)
		return s

	def __str__(self):
		return unicode(self).encode('utf-8')


class RssChannelItem(RssBase):
	def __init__(self, item_element):
		if item_element.tag != 'item':
			raise Exception('Wrong element in constructor.')
		self.element = item_element

		# All elements of an item are optional
		# See http://www.rssboard.org/rss-specification#hrelementsOfLtitemgt
		fields = ['title', 'link', 'description', 'author', 'category', 'comments', 'enclosure', 'guid', 'pubDate', 'source']
		for field in fields:
			self.update_field(field)


class RssChannel(RssBase):
	def __init__(self, chan_element):
		if chan_element.tag != 'channel':
			raise Exception('Wrong element in constructor.')
		self.element = chan_element

		# The required elements
		# See http://www.rssboard.org/rss-specification#requiredChannelElements
		fields = ['title', 'link', 'description']
		for field in fields:
			self.update_field(field)

		self.items = list()
		for item in self.element.findall('item'):
			self.items.append(RssChannelItem(item))


class Rss(RssBase):
	def __init__(self, rss_element):
		if rss_element.tag != 'rss':
			raise Exception('Wrong element in constructor.')
		self.element = rss_element
		self.channel = RssChannel(self.element.find('channel'))


def parse_string(xml):
	return Rss(etree.XML(xml))