import urllib

extra_output = False

def fetch_url(url):
	try:
		res = urllib.urlopen(url)
		if extra_output: print('Info. HTTP status code: %i' % (res.getcode()))
		data = res.read()
		#print('Data: %s' % (data))
		if res.getcode() == 200:
			return data
		else:
			if extra_output:
				print('Warning. Server responds with HTTP status code %i. Ignoring the data.' % (res.getcode()))
			return None
	except IOError:
		print('Error. Can\'t read the specified URL: %s' % (url))
		return None

if __name__ == '__main__':
	import sys
	extra_output = True
	print('Response:\n%s' % (fetch_url(sys.argv[1], verbose=True)))